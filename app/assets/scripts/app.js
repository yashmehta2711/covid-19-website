import $ from 'jquery';
import "lazysizes";
import "../styles/style.css";
new WOW().init();

//JS FOR SLIDESHOW
var slideshows = document.querySelectorAll('[data-component="slideshow"]');
slideshows.forEach(initSlideShow);
function initSlideShow(slideshow) {
    var slides = document.querySelectorAll(`#${slideshow.id} [role="list"] .slide`);
    var index = 0, time = 3000;
    slides[index].classList.add('active');  
    setInterval( () => {
        slides[index].classList.remove('active');
        index++;
        if (index === slides.length) index = 0; 
        slides[index].classList.add('active');
    }, time);
}



//JS FOR NAVBAR
$(document).ready(function(){
  $(window).scroll(function(){
  	var scroll = $(window).scrollTop();
	  if (scroll > 50) {
	    $(".navbar").css("background" , "rgb(39, 71, 130)");
	    $(".menu-item a").css("color" , "#FFF"); 
        document.getElementById("navImg").src = "./assets/images/TP.png";
	  }else{
		  $(".navbar").css("background" , "#F6F9FE");  	
		  $(".menu-item a").css("color" , "#274782"); 
          document.getElementById("navImg").src = "./assets/images/TP1.png";	
	  }
  })
})



// JS FOR FLOATING
$(document).ready(function(){
  $(window).scroll(function(){
  	var scroll = $(window).scrollTop();
	  if (scroll > 900) {
          $(".float").css("visibility" , "visible");
	  }else{
          $(".float").css("visibility" , "hidden");
      }
      if (scroll > 8820) {
          $(".float").css("background" , "rgb(221, 75, 48)");
	  }else{
          $(".float").css("background" , "rgb(39, 71, 130)");
      }
  })
})



//JS FOR PRE-LOADER
//Progress Bar
var i = 0;
function move() {
  if (i == 0) {
    i = 1;
    var elem = document.getElementById("myBar");
    var width = 1;
    var id = setInterval(frame, 29.5);
    function frame() {
      if (width >= 100) {
        clearInterval(id);
        i = 0;
      } else {
        width++;
        elem.style.width = width + "%";
      }
    }
  }
}
//Phrases
var strings = [
    'Your Nose And Ears Continue Growing Throughout Your Entire Life.',
    'When Listening To Music, Your Heartbeat Will Sync With The Rhythm.',
    'Skin Is The Human Body’s Largest Organ.',
    'The Small Intestine Is Roughly 23 Feet Long.',
    'As Well As Having Unique Fingerprints, Humans Also Have Unique Tongue Prints.',
    'You can’t breathe and swallow at the same time.',
    'While Awake, Your Brain Produces Enough Electricity To Power A Lightbulb.',
    'In Camera Terms, The Human Eye Is About 576 Megapixels.',
    'Humans Are The Only Animals With Chins.',
    'Belly Buttons Grow Special Hairs To Catch Lint.',
    'The sound Of Cracking Your Knuckles Comes From Gas Bubbles Bursting In Your Joints.',
    'Thumbs Have Their Own Pulse.',
    'On A Genetic Level, All Human Beings Are More Than 99 Percent Identical.',
    'The Foot Is One Of The Most Ticklish Parts Of The Body.',
    'Blushing Is Caused By A Rush Of Adrenaline.',
    'Eyes Are So Powerful They Can Detect Light From A Candle Flame Over 1.7 Miles Away.',
    'Sneezing Might Be Our Nose’s Way Of “Rebooting” Itself To Get Rid Of All The Bad Particles We’ve Inhaled.',
    'There Are Over 100,000 Miles Of Blood Vessels Inside Your Body.',
    'Without Your Pinky Finger, You Would Lose About 50% Of Your Hand Strength.',
    'Women Have A Stronger Sense Of Smell Than Men.',
    'Womens Hearts Beat Faster Than Mens.',
    'Right-handed People Live, On Average, Nine Years Longer Than Left-handed People.',
    'Only 7% Of People Are Left-handed.',
    'Fingernails Grow About Four Times Faster Than Your Toenails.',
    'It Is Impossible To Sneeze With Your Eyes Open.',
    'A Sneeze Can Travel Up To 99 Miles Per Hour.',
    'The Human Body Is Made Up Of 0.2 Milligrams Of Gold.',
    'Your Nose Can Remember 50,000 Different Scents.',
    'Babies Are Born Without Kneecaps. They Dont Appear Until The Age Of Two.',
    'The Average Human Beings Swallow One Liter Of Saliva A Day.',
    'There Is A Protein In The Body Called Pikachu Rim.',
    'Every Person On This Planet Has An Individual Smell.',
    '50 Billion Cells Die In Your Body Everyday.',
    'New Cells Regenerate At 25 Million New Cells Per Second.',
    'Bones Are 50 Times Lighter Than Steel But Just As Strong.',
    'Bones Only Account For 14% Of Our Total Weight.',
    'The Weakest Of Your Five Senses Is Your Sense Of Smell.',
    'The Brain Receives Nerve Impulses Of Speeds Up To 240Km/Hour',
    'Each Day Human Lose Half A Liter Of Water Just Through Breathing.',
    'A Full Human Bladder Can Hold 600 Milliliters Of Liquid.',
    'Our Body Produces Enough Heat In 30 Minutes To Boil Half A Gallon Of Water.',
    'There Are 1,00,000 Miles Of Blood Vessels In An Adult Human Body.',
    'The Average Red Blood Cell Only Lives For 120 Days.',
    'There Are 45 Miles Of Nerves In Human Skin.',
    '7,000,000,000,000,000,000,000,000 (7 Octillion) Atoms Make A Single Human Body.',
    'Some Of The Atoms That Make Up Your Body Are The Same Atoms That Were A Part Of The Big Bang.',
    'Adult Lungs Have A Surface Area Of Around 70 Square Meters.',
    'Your Nose And Ears Continue Growing Throughout Your Entire Life.',
    'The Brain Uses Over A Quarter Of The Oxygen Used By The Human Body.',
    'Skin Is The Human Bodys Largest Organ.'
];
var rand = strings[Math.floor(Math.random() * strings.length)];
document.getElementById('loading-text').innerHTML = rand;
$(window).on('load', function () {
    $(".loader-wrapper").delay(3000).fadeOut('slow');
    move();
});



//JS FOR BUTTON 1
$(".send1").on('click', function(){
    $(".text1").addClass("active1");
    $(".send1").addClass("active1");
    $(".loader1").addClass("active1");
    
    $(".send1").delay(1700).queue(function(){
        $(this).addClass("finished1").clearQueue();
    });

    $(".done1").delay(1600).queue(function(){
        $(this).addClass("active1").clearQueue();
    });
})



//JS FOR BUTTON 2
$(".send2").on('click', function(){
    $(".text2").addClass("active2");
    $(".send2").addClass("active2");
    $(".loader2").addClass("active2");
    
    $(".send2").delay(1700).queue(function(){
        $(this).addClass("finished2").clearQueue();
    });

    $(".done2").delay(1600).queue(function(){
        $(this).addClass("active2").clearQueue();
    });
})



//JS FOR BUTTON 3
$(".send3").on('click', function(){
    $(".text3").addClass("active3");
    $(".send3").addClass("active3");
    $(".loader3").addClass("active3");
    
    $(".send3").delay(1700).queue(function(){
        $(this).addClass("finished3").clearQueue();
    });

    $(".done3").delay(1600).queue(function(){
        $(this).addClass("active3").clearQueue();
    });
})



//JS FOR BUTTON 4
$(".send4").on('click', function(){
    $(".text4").addClass("active4");
    $(".send4").addClass("active4");
    $(".loader4").addClass("active4");
    
    $(".send4").delay(1700).queue(function(){
        $(this).addClass("finished4").clearQueue();
    });

    $(".done4").delay(1600).queue(function(){
        $(this).addClass("active4").clearQueue();
    });
})



//JS FOR BUTTON 5
$(".send5").on('click', function(){
    $(".text5").addClass("active5");
    $(".send5").addClass("active5");
    $(".loader5").addClass("active5");
    
    $(".send5").delay(1700).queue(function(){
        $(this).addClass("finished5").clearQueue();
    });

    $(".done5").delay(1600).queue(function(){
        $(this).addClass("active5").clearQueue();
    });
})